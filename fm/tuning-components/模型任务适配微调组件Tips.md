# 模型任务适配微调组件Tips
微调组件在 ModelArts 拉起模型任务的流程如下：

![process.png](process.png)

## 模型任务上云引入的下载与同步

结合引擎包（mxEngineKit）对 ModelArts SDK 的特定调用以及启动包（mxLaunchKit），保证了 OBS 与 ModelArts 训练作业所在容器本地路径间的同步。app_config 中定义 OBS 路径到训练作业容器本地的路径映射关系如下：

![path_mapping.png](path_mapping.png)

1. ModelArts 将 app_config 中 code_url，data_path 对应的 OBS 文件夹在训练作业初始化时，下载到了容器本地
2. 任务运行过程中，ModelArts 将以定时回传的形式在 app_config 中 log_path 指定的 OBS 文件夹下生成一系列以训练任务 uuid 为名称前缀的日志文件夹及日志文件
3. 任务运行过程中，ModelArts 将以定时回传的形式同步用户在本地输出目录的文件到 app_config 中 output_path 参数指定的 OBS 路径
4. mxLaunchKit 会将 app_config 中 pretrained_model_path，ckpt_path 以及 model_config 参数对应的 OBS 文件夹或文件下载到容器本地
5. mxLaunchKit 会将 mxTuningKit 调用用户脚本产生的日志文件，以定时回传的形式同步至第二点中提到的由 ModelArts 生成的日志文件夹下

## 模型任务上云涉及适配项

1. 模型代码应当去除基于相对路径的路径引用

```
# 在启动脚本获取所在目录的绝对路径
abs_path = os.path.abspath(os.path.dirname(os.path.abspath(__file__)))
```

2. 模型的启动脚本只需要关注经过引擎包与启动包转换，传递给工具包（mxTuningKit）的参数即可，即模型代码启动脚本，需要具备解析以下固定传递参数的能力

```
data_path, output_path, pretrained_model_file_path/ckpt_path
```

3. 模型代码涉及超参的，可以在 model_config 中定义，mxTuningKit 会进行解析并以参数形式拼接传递给模型代码启动脚本

```
# model_config.yaml
params:
    A: a
    B: b
# 最终启动命令
python xxx.py 固定参数 --A a --B b
```

4. 若需要进行单机多卡或多机多卡的分布式训练，模型代码自身需要实现分布式逻辑
5. pretrained_model_file_path/ckpt_path 均为文件夹路径，若需要指定模型文件可以结合 model_config 定义具体文件名，并在启动脚本中解析使用

## 模型任务上云注意事项

1. 由于云上会将数据、模型文件以及用户代码分别进行下载。不要将数据或模型文件放在模型代码的 OBS 路径下，防止重复下载
2. 多机多卡场景下，单个节点的规格必须选择八卡
3. 所有训练任务相关的日志文件或日志文件夹命名格式：modelarts-job-{uuid}。其中 uuid 是 ModelArts 为模型任务对应训练作业分配的作业 ID
4. mxTuningKit 传递给用户启动脚本的 output_path 会在原来的基础上增加后缀，防止写冲突

```
# 此处 uuid 为随机产生
/cache/cache_ouput/TK_{uuid}
```

5. 当前提供的下游任务适配示例（如 caption 及 VQA）中的评估及推理作业只支持单机单卡
6. 当前提供的下游任务适配示例中的微调作业，提供 use_parallel 参数作为分布式训练开关，需要在 model_config 中指定
7. mxTuningKit 只会打印零号卡的日志，其余卡的日志会落盘并同步至 app_config 中 log_path 指定的 OBS 文件夹

```
# log_path: obs://HwAiAUser/log/
obs://HwAiUser/log/modelarts-job-{uuid}/mxTuningKit
```

8. 根据 ModelArts 新版训练镜像制作示例构建的镜像，通过修改 fmk.py 开启了 CANN 层日志罗盘

```
# log_path: obs://HwAiAUser/log/
obs://HwAiUser/log/modelarts-job-{uuid}/ascend
```

9. 结合 fmk.py 的修改，启动包（mxLaunchKit）会 mxTuningKit 的工作目录切换至 code_path

```
# code_path: obs://HwAiAUser/code/
/home/ma-user/user-job-dir/code/
```
