# **MS-GameTutorial：MindSpore模型开发者实战指导手册**
欢迎使用MindSpore（中文名：昇思）完成模型开发和迁移任务！本手册针对MindSpore普通网络模型与超大网络模型的开发迁移过程提供了详实的入门样例、多样化的调优工具使用方法和便于查找的问题清单，希望能帮到您。与官方提供的文档相比，本手册更侧重于工程实施过程中所遇到的实际问题，是对官方文档实现的细节补充。也正因为于此，非常欢迎您的加入与我们一同完善MindSpore模型开发与迁移流程。😊

## **普通网络模型部分**

### **1.昇腾全栈一览与实践**
- 昇腾全栈介绍
- 昇思框架介绍
- 本地环境配置
- 快速上手案例
- 问题清单

### **2.对象储存服务(OBS)相关**
- 对象存储服务简介
- OBS工具介绍
- 快速上手
- 问题清单

### **3.ModelArts相关**
- [新版训练作业使用简介](https://gitee.com/xiaobai-666/MS-GameTutorial/tree/master/gm/modelarts)
- [示例：使用 OBS 和 ModelArts 完成训练作业部署（LeNet+MNIST数据集）](https://gitee.com/xiaobai-666/MS-GameTutorial/tree/master/gm/modelarts)
- [示例：从 0 到 1 制作自定义镜像并用于训练（MindSpore+Ascend）](https://support.huaweicloud.com/docker-modelarts/develop-modelarts-0106.html)
- 问题清单

### **4.PyTorch模型迁移MindSpore相关**
- 工具说明
- 问题清单

### **5.模型性能调优相关**
- 昇思性能调优工具介绍
- [MindInsight的安装与启动](https://www.mindspore.cn/mindinsight/docs/zh-CN/r1.8/mindinsight_commands.html)
- [ProFiler文件的生成与加载](https://support.huaweicloud.com/develop-modelarts/develop-modelarts-0068.html#develop-modelarts-0068__section1369813163528)
- ProFiler的分析
- 问题清单

### **6.模型精度调优相关**
- 精度异常的来源分析
- 精度调优定位的思路
- 精度调优的工具：Print、数值校验
- 官网相关材料
  - [精度调优指南](https://www.mindspore.cn/mindinsight/docs/zh-CN/r1.8/accuracy_problem_preliminary_location.html)
  - [精度调优资料分享](https://www.mindspore.cn/docs/zh-CN/r1.8/faq/precision_tuning.html)
- 问题清单

### **7.报错定位分析思路**
- 如何阅读报错、报错定界
- 官网相关材料
  - [功能调试指南](https://www.mindspore.cn/tutorials/experts/zh-CN/r1.8/debug/function_debug.html)
  - [报错分析案例](https://www.mindspore.cn/tutorials/experts/zh-CN/r1.8/debug/error_analyze.html)
  - [FAQ](https://www.mindspore.cn/docs/zh-CN/r1.8/faq/installation.html)
- 问题清单

## **超大网络模型部分**

### **1.大模型基础内容介绍**
- 大模型基本概念及综述介绍
- 基于昇腾软硬件的多模态大模型介绍


### **2.模型并行策略与案例展示**
- 模型并行策略
- 应用案例
- 问题清单
- [分布式策略异常的典型报错](https://www.mindspore.cn/tutorials/experts/zh-CN/r1.8/debug/error_analyze.html#%E5%88%86%E5%B8%83%E5%BC%8F%E5%B9%B6%E8%A1%8C%E9%94%99%E8%AF%AF%E5%88%86%E6%9E%90)



## 说明 
各部分中的问题清单收录了在MindSpore开发和迁移过程中可能遇到的报错和对应的解决方法。本文档的使用人也可对遇到的问题评估，认为是较为普遍的问题可以申请添加到对应清单中。  