# -*- coding: utf-8 -*-
"""
功能: 手写体识别模型训练脚本（ModelArts新版训练）
"""

import os
import argparse

import mindspore as ms
import mindspore.nn as nn

import mindspore.dataset as ds
from mindspore.dataset.vision import Resize, Rescale, HWC2CHW, Inter
from mindspore.dataset.transforms import TypeCast
from mindspore import dtype as ms_type

# 外部入参解析器定义
parser = argparse.ArgumentParser()
parser.add_argument("--data_path", type=str)
parser.add_argument("--output_path", type=str)
parser.add_argument("--epoch", type=int)


def create_dataset(data_path, batch_size=32):
    # 使用 MindSpore 预置的手写体数据集类读取数据
    mnist_ds = ds.MnistDataset(data_path)

    # 图片大小调整适配 LeNet，确保卷积后输出 28 * 28 的特征图
    resize_op = Resize((32, 32), interpolation=Inter.LINEAR)
    # 数据标准化 (均值，方差)
    rescale_op = Rescale(1.0 / 255.0, 0.0)
    rescale_nml_op = Rescale(1 / 0.3081, -1 * 0.1307 / 0.3081)
    # 转置
    hwc2chw_op = HWC2CHW()
    # 标签列类型转换
    type_cast_op = TypeCast(ms_type.int32)

    # 在数据集上应用定义的映射
    mnist_ds = mnist_ds.map(operations=type_cast_op, input_columns="label")
    mnist_ds = mnist_ds.map(operations=[resize_op, rescale_op, rescale_nml_op, hwc2chw_op], input_columns="image")

    # shuffle以及数据批量设置
    buffer_size = 10000
    mnist_ds = mnist_ds.shuffle(buffer_size=buffer_size)
    mnist_ds = mnist_ds.batch(batch_size)
    return mnist_ds


class LeNet5(nn.Cell):
    """
    LeNet-5网络结构
    """
    def __init__(self, num_class=10, num_channel=1):
        super(LeNet5, self).__init__()
        # 卷积层，输入的通道数为num_channel，输出的通道数为6，卷积核大小为5*5
        self.conv1 = nn.Conv2d(num_channel, 6, 5, pad_mode='valid')
        # 卷积层，输入的通道数为6，输出的通道数为16，卷积核大小为5*5
        self.conv2 = nn.Conv2d(6, 16, 5, pad_mode='valid')
        # 全连接层，输入个数为16*5*5，输出个数为120
        self.fc1 = nn.Dense(16 * 5 * 5, 120)
        # 全连接层，输入个数为120，输出个数为84
        self.fc2 = nn.Dense(120, 84)
        # 全连接层，输入个数为84，分类的个数为num_class
        self.fc3 = nn.Dense(84, num_class)
        # ReLU激活函数
        self.relu = nn.ReLU()
        # 池化层
        self.max_pool2d = nn.MaxPool2d(kernel_size=2, stride=2)
        # 多维数组展平为一维数组
        self.flatten = nn.Flatten()

    def construct(self, x):
        # 使用定义好的运算构建前向网络
        x = self.conv1(x)
        x = self.relu(x)
        x = self.max_pool2d(x)
        x = self.conv2(x)
        x = self.relu(x)
        x = self.max_pool2d(x)
        x = self.flatten(x)
        x = self.fc1(x)
        x = self.relu(x)
        x = self.fc2(x)
        x = self.relu(x)
        x = self.fc3(x)
        return x


if __name__ == "__main__":
    # 外部入参解析
    args = parser.parse_known_args()[0]

    # 创建训练、评估数据集
    dataset_train = create_dataset(os.path.join(args.data_path, "train"), 32)
    dataset_eval = create_dataset(os.path.join(args.data_path, "test"), 32)
    # 创建网络模型
    network = LeNet5(num_class=10)
    # 定义损失函数
    net_loss = nn.SoftmaxCrossEntropyWithLogits(sparse=True, reduction='mean')
    # 定义优化器函数
    net_opt = nn.Momentum(network.trainable_params(), learning_rate=0.01, momentum=0.9)
    # 设置模型保存参数，模型训练保存参数的step为1875。
    config_ck = ms.CheckpointConfig(save_checkpoint_steps=1875, keep_checkpoint_max=10)
    # 应用模型保存参数
    checkpoint = ms.ModelCheckpoint(prefix="lenet", directory=args.output_path, config=config_ck)
    # 初始化模型参数
    model = ms.Model(network, loss_fn=net_loss, optimizer=net_opt, metrics={'accuracy'})

    # 训练网络模型，并保存为lenet-x_1875.ckpt文件
    model.train(args.epoch, dataset_train, callbacks=checkpoint)
    acc = model.eval(dataset_eval)
    print("{}".format(acc))
