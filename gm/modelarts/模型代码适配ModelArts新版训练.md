# 模型代码适配ModelArts新版训练

### 训练代码

训练任务对应的容器内存在任务代码缓存路径

```
MA_JOB_DIR=/home/ma-user/modelarts/user-job-dir
```

创建训练任务时指定的代码存放 OBS 目录，会在训练任务启动时被下载至缓存路径

```
obs://xxx/path_to_code/      ------>     ${MA_JOB_DIR}/path_to_code/
```

### 输入及输出

创建训练任务时可以添加训练输入与输出，可自定义参数的 key 值。ModelArts 会一次性拉取输入，并自动定时同步输出路径

```
# key=data_path
obs://xxx/path_to_input/     ------>     /home/ma-user/modelarts/inputs/data_path_0
# key=output_path
obs://xxx/path_to_input/     ------>     /home/ma-user/modelarts/outputs/output_path_0
```

参数的值当前由 ModelArts 默认生成，最后会根据参数排序增加后缀。

```
# key=first
obs://xxx/path_to_input/     ------>     /home/ma-user/modelarts/inputs/first_0
# key=second
obs://xxx/path_to_input/     ------>     /home/ma-user/modelarts/outputs/second_1
```

### 相对引用

* 对于路径的相对引用：建议切换为绝对路径
* 对于模块的相对引用（自定义命令场景）：进入正确的工作目录后，再执行启动脚本

### 参数传递

ModelArts 会在指定的启动命令后根据超参、输入及输出的配置，自定拼接任务需要的参数

### 日志

标准输出的日志会动态回传至创建训练任务时指定的作业日志 OBS 路径

### 分布式训练

创建任务时通过选择计算节点个数以及节点的规格来确定分布式训练配置。**注意多机情况下，当前 ModelArts 只支持选择八卡规格**

* 计算节点个数为2，规格选择 8 * Ascend 910(32GB)：两机八卡
* 计算节点个数为1，规格选择 4 * Ascend 910(32GB)：单机四卡

自定义镜像场景，ModelArts 基于分布式训练配置直接拉起训练任务的工具，具体参考以下说明

```
# 示例：从 0 到 1 制作自定义镜像并用于训练（MindSpore+Ascend）
# 可能需要增加部分代码来打开 CANN 包日志打印与启动工作目录切换
https://support.huaweicloud.com/docker-modelarts/develop-modelarts-0106.html
# 最终的启动命令
python path_to_tool/run_ascend910.py python ${MA_JOB_DIR}/demo-code/xxx.py
```

### 示例

lenet-5 + 手写体数据集（obs://wuhy/mav2/mnist-lenet-demo）

* 启动脚本
```
# 本地运行
mnist.py
# ModelArts新版训练运行
mnist_4mav2.py
```

* 数据集文件树（http://yann.lecun.com/exdb/mnist/）

```
./data/
├── test
│   ├── t10k-images-idx3-ubyte
│   └── t10k-labels-idx1-ubyte
└── train
    ├── train-images-idx3-ubyte
    └── train-labels-idx1-ubyte
```

* 路径对照关系

|      | OBS 路径                                 | 容器内路径                                     |
| ---- | ---------------------------------------- | ---------------------------------------------- |
| 代码 | obs://mav2-adaptation/mnist-lenet-demo/code/   | /home/ma-user/modelarts/user-job-dir/code/     |
| 输入 | obs://mav2-adaptation/mnist-lenet-demo/data/   | /home/ma-user/modelarts/inputs/data_path_0/    |
| 输出 | obs://mav2-adaptation/mnist-lenet-demo/output/ | /home/ma-user/modelarts/outputs/output_path_0/ |
| 日志 | obs://mav2-adaptation/mnist-lenet-demo/log/    | /home/ma-user/modelarts/log/                   |

* 增加参数解析

```
import argparse


parser = argparse.ArgumentParser()
parser.add_argument("--data_path", type=str)
parser.add_argument("--output_path", type=str)
parser.add_argument("--epoch", type=int)

args = parser.parse_known_args()[0]

# load data
dataset_train = create_dataset(os.path.join(args.data_path, "train"), 32)

# save checkpoint
checkpoint = ms.ModelCheckpoint(prefix="lenet", directory=args.output_path, config=config_ck)

# hyper-parameter: epoch
model.train(args.epoch, dataset_train, callbacks=checkpoint)

```

* 启动命令

```
python /home/ma-user/ascend910/run_ascend910.py python ${MA_JOB_DIR}/code/mnist_4mav2.py
```

* 参数传递

```
python /home/ma-user/ascend910/run_ascend910.py python ${MA_JOB_DIR}/code/mnist_4mav2.py --epoch=10 --data_path=/home/ma-user/modelarts/inputs/data_path_0/ --output_path=/home/ma-user/modelarts/outputs/output_path_0/
```
